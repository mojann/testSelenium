import com.thoughtworks.selenium.Selenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertEquals;

public class TestCaseEssaiDeux {
	private Selenium selenium;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\tomda\\Documents\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		String baseUrl = "http://localhost:1047/";
		selenium = new WebDriverBackedSelenium(driver, baseUrl);
	}

	@Test
	public void testCaseEssai() throws Exception {
		selenium.open("/");
		assertEquals("Accueil hein", selenium.getTitle());
		assertEquals("Salut", selenium.getText("id=myH3"));
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
